import { Geolocation } from './geolocation';
import { CarSpace } from './car-space';

export class Estacionamento{

    constructor(public id : string,public name : string,public geolocation : Geolocation,public carSpaces : CarSpace[]){}

}