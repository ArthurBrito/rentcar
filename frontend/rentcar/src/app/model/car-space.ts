import { CarSpaceType } from './car-space-type';

export class CarSpace {

    constructor(public id : string,public position : string,public isBusy : Boolean,public type : CarSpaceType){}

}