import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstacionamentoComponent } from './estacionamento/estacionamento.component';
import { VagasComponent } from './vagas/vagas.component';
import { TicketComponent } from './ticket/ticket.component';

const routes: Routes = [
  { path: 'estacionamento', component: EstacionamentoComponent },
  { path: 'estacionamento/:id/vagas', component: VagasComponent },
  { path: 'Ticket', component: TicketComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
