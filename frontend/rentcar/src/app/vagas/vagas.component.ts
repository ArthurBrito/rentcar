import { Component, OnInit } from '@angular/core';
import { EstacionamentoService } from '../estacionamento/estacionamento.service';
import { ActivatedRoute } from '@angular/router';
import { CarSpace } from '../model/car-space';
import { Estacionamento } from '../model/estacionamento';

@Component({
  selector: 'app-vagas',
  templateUrl: './vagas.component.html',
  styleUrls: ['./vagas.component.css']
})
export class VagasComponent implements OnInit {

  vagasDisponiveis : number;

  carSpaces : CarSpace[];

  estacionamento : Estacionamento;

  constructor(private service : EstacionamentoService, private router: ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(
      params => {
        let id : number = params["id"]

        this.service.getCarSpacesAvaliableCount(id).subscribe(
          (successResult : number) => {
            console.log(successResult);
            this.vagasDisponiveis = successResult;
            console.log(successResult);
          }, (errorResult) => {
            console.log(errorResult)
          }
        )

        this.service.get(id).subscribe(
          (successResult : Estacionamento) => {
            console.log(successResult);
            this.estacionamento = successResult;
            this.carSpaces = successResult.carSpaces;
            console.log(this.carSpaces);
          }, (errorResult) => {
            console.log(errorResult)
          }
        )

      }
    )
  }

  parkCar(event, carspace : CarSpace){
    this.service.parkCar(this.estacionamento.id, carspace.position).subscribe(
      (successResult : Estacionamento) => {
        console.log(successResult);
      }, (errorResult) => {
        console.log(errorResult)
      }
    );
  }

  unparkCar(event, carspace : CarSpace){
    this.service.unparkCar(this.estacionamento.id, carspace.position).subscribe(
      (successResult : Estacionamento) => {
        console.log(successResult);
      }, (errorResult) => {
        console.log(errorResult)
      }
    );
  }

}
