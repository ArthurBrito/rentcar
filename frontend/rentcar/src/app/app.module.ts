import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EstacionamentoComponent } from './estacionamento/estacionamento.component';
import { EstacionamentoService } from './estacionamento/estacionamento.service';

import { HttpClientModule } from '@angular/common/http';
import { VagasComponent } from './vagas/vagas.component';
import { TicketComponent } from './ticket/ticket.component';

@NgModule({
  declarations: [
    AppComponent,
    EstacionamentoComponent,
    VagasComponent,
    TicketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    EstacionamentoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
