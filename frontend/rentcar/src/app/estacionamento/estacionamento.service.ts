import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Estacionamento } from '../model/estacionamento';
import { ParkCarRequest } from '../model/park-car-request';

@Injectable()
export class EstacionamentoService { 

    //private baseUrl : string = "https://localhost:5001/api/Parking";
    private baseUrl : string = "https://rentcar-api.azurewebsites.net/api/Parking";

    headers : HttpHeaders;

    constructor(private _http: HttpClient) { 

      this.headers = new HttpHeaders({ 'Content-Type': 'application/json-patch+json' });

    }

    getAll() {
      return this._http.get(this.baseUrl);
    }
  
    get(userId : number) {
      return this._http.get(`${this.baseUrl}/${userId}`);
    }
  
    getCarSpacesAvaliableCount(userId : number){
      return this._http.get(`${this.baseUrl}/${userId}/CarSpacesAvaliableCount`);
    }

    create(user : Estacionamento) {
      return this._http.post(`${this.baseUrl}`,user)
    }
  
    update(userId : number, user : Estacionamento) {
      return this._http.put(`${this.baseUrl}/${userId}/update`, user);
    }
  
    delete(userId : number) {
      return this._http.delete(`${this.baseUrl}/${userId}/delete`);
    }

    parkCar(estacionamentoId : string, position : String) {
      return this._http.put(`${this.baseUrl}/${estacionamentoId}/parkCar`, new ParkCarRequest(position), {headers: this.headers});
    }

    unparkCar(estacionamentoId : string, position : string) {
      return this._http.put(`${this.baseUrl}/${estacionamentoId}/unparkCar`, new ParkCarRequest(position), {headers: this.headers});
    }

}