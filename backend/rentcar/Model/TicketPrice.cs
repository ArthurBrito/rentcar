﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentcar.Model
{
    public class TicketPrice : BaseEntity
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        public TimeSpan time { get; set; }

        public String timeDescription { get; set; }

        public Double Value { get; set; }

        public TicketPrice(TimeSpan time, String timeDescription, double value)
        {
            this.time = time;
            this.timeDescription = timeDescription;
            Value = value;
        }

    }
}
