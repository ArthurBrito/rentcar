﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentcar.Model.VO
{
    public class TicketReportVO
    {

        public Double totalPayments { get; set; }

        public IEnumerable<IGrouping<int, Ticket>> paymentsByHours { get; set; }

        public TicketReportVO(double totalPayments, IEnumerable<IGrouping<int, Ticket>> paymentsByHours)
        {
            this.totalPayments = totalPayments;
            this.paymentsByHours = paymentsByHours;
        }

    }
}
