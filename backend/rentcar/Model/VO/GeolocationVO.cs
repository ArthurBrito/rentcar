﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar.Model.VO
{
    public class GeolocationVO
    {

        public Double Latitude { get; set; }

        public Double Longitude { get; set; }

    }
}
