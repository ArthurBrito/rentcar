﻿namespace rentcar.Model.Enum
{
    public enum CarSpaceType
    {

        NORMAL,

        PREFERENTIAL,

        ELDER_SPACE

    }
}
