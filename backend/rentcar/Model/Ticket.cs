﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar.Model
{
    public class Ticket : BaseEntity
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        public Double Price { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public DateTime PaymentTime { get; set; }

        public String ParkingId { get; set; }

        public Ticket(DateTime startDateTime, DateTime endDateTime, DateTime paymentTime, String parkingId)
        {
            StartDateTime = startDateTime;
            EndDateTime = endDateTime;
            PaymentTime = paymentTime;
            ParkingId = parkingId;
        }

        public TimeSpan GetParkingTime()
        {
            DateTime start = this.StartDateTime;
            DateTime end = DateTime.MaxValue.Equals(this.EndDateTime) ? DateTime.Now : this.EndDateTime;

            return start - end;
        }

    }
}
