﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using rentcar.Model.VO;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar.Model
{
    public class Parking : BaseEntity
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        public String Name { get; set; }

        public GeolocationVO Geolocation { get; set; }

        public List<CarSpace> CarSpaces { get; set; }

        public Parking(string name, GeolocationVO geolocation, List<CarSpace> carSpaces)
        {
            Name = name;
            Geolocation = geolocation;
            CarSpaces = carSpaces;
        }

    }
}
