﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using rentcar.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar.Model
{
    public class CarSpace : BaseEntity
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        public String Position { get; set; }

        public Boolean IsBusy { get; set; }

        public CarSpaceType Type { get; set; }

        public String TicketId { get; set; }

        public CarSpace(String position, Boolean isBusy, CarSpaceType type, String ticketId)
        {
            Position = position;
            IsBusy = isBusy;
            Type = type;
            TicketId = ticketId;
        }

    }
}
