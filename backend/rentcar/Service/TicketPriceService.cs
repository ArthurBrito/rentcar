﻿using rentcar.Model;
using rentcar.Repositories;
using rentcar.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentcar.Service
{
    public class TicketPriceService : BaseService<TicketPrice>
    {

        public TicketPriceService(IRepository<TicketPrice> repository) : base(repository)
        {
        }

        public TicketPrice GetByTimeDescription(String timeDescription)
        {
            return (this.repository as TicketPriceRepository).GetByTimeDescription(timeDescription);
        }

    }
}
