﻿using Microsoft.AspNetCore.Mvc;
using rentcar.Model;
using rentcar.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentcar.Service.Contract
{
    public interface IService<T> where T : BaseEntity
    {

        IEnumerable<T> GetAll();

        T Get(String id);

        T Insert(T item);

        T Update(String id, T NewItem);

        void Delete(String id);
        
    }
}
