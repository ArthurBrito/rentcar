﻿using rentcar.Repositories.Contract;
using rentcar.Model;
using System.Collections.Generic;
using System.Linq;

namespace rentcar.Service
{
    public class CarSpaceService : BaseService<CarSpace>
    {
        public CarSpaceService(IRepository<CarSpace> repository) : base(repository)
        {
        }

        public IEnumerable<CarSpace> GetNotBusyCarSpaces()
        {

            IEnumerable<CarSpace> carSpaces = this.repository.GetAll();

            return (from cs in carSpaces
                    where cs.IsBusy
                    select cs);

        }

    }
}
