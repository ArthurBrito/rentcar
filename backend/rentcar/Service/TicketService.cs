﻿using rentcar.Repositories.Contract;
using rentcar.Model;
using System;
using rentcar.Service.Contract;
using System.Collections.Generic;
using rentcar.Repositories;

namespace rentcar.Service
{
    public class TicketService : BaseService<Ticket>
    {
        private TicketPriceService ticketPriceService;

        public TicketService(IRepository<Ticket> repository, TicketPriceService ticketPriceService) : base(repository)
        {
            this.ticketPriceService = ticketPriceService;
        }

        public Ticket PayTicket(string id, double paymentValue)
        {
            Ticket ticket = this.Get(id);

            TimeSpan parkingTime = ticket.GetParkingTime();

            TicketPrice baseTicketPrice = this.ticketPriceService.GetByTimeDescription("Até 3 horas");

            if (parkingTime.TotalHours <= baseTicketPrice.time.TotalHours)
            {
                ticket.Price = baseTicketPrice.Value;
            }
            else
            {
                TicketPrice extraTicketPrice = this.ticketPriceService.GetByTimeDescription("Hora extra");

                TimeSpan diffTime = parkingTime - baseTicketPrice.time;

                Double extraValue = diffTime.TotalHours * extraTicketPrice.Value;

                ticket.Price = baseTicketPrice.Value + extraValue;
            }

            ticket.PaymentTime = DateTime.Now;

            this.Update(id, ticket);

            return ticket;
        }

        public List<Ticket> GetTicketsOfParking(String id)
        {
            return (this.repository as TicketRepository).GetTicketsOfParking(id);
        }
    }
}
