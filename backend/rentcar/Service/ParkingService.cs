﻿using rentcar.Repositories.Contract;
using rentcar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using rentcar.Service.Contract;
using Microsoft.AspNetCore.Mvc;
using rentcar.Model.VO;

namespace rentcar.Service
{
    public class ParkingService : BaseService<Parking>
    {
        private IService<CarSpace> carSpaceService;
        private IService<Ticket> ticketService;

        public ParkingService(IRepository<Parking> repository, IService<CarSpace> carSpaceService, IService<Ticket> ticketService) : base(repository)
        {
            this.carSpaceService = carSpaceService;
            this.ticketService = ticketService;
        }

        public override Parking Insert(Parking item)
        {

            foreach (CarSpace carSpace in item.CarSpaces)
            {
                if (carSpace.Id == null || carSpace.Id == String.Empty)
                {
                    this.carSpaceService.Insert(carSpace);
                } else
                {
                    this.carSpaceService.Update(carSpace.Id , carSpace);
                }
            }

            return this.repository.Insert(item);
        }

        public int CarSpacesAvaliableCount(string id)
        {
            Parking parking = this.Get(id);

            return (from cs in parking.CarSpaces
                    where !cs.IsBusy
                    select cs).Count();
        }

        public override Parking Update(string id, Parking NewItem)
        {

            foreach (CarSpace carSpace in NewItem.CarSpaces)
            {
                if (carSpace.Id == null || carSpace.Id == String.Empty)
                {
                    this.carSpaceService.Insert(carSpace);
                }
                else
                {
                    this.carSpaceService.Update(carSpace.Id, carSpace);
                }
            }

            return this.repository.Update(id, NewItem);
        }

        public ActionResult<TicketReportVO> PaymentReport(string id)
        {

            List<Ticket> tickets = (this.ticketService as TicketService).GetTicketsOfParking(id).Where(t => !DateTime.MaxValue.Equals(t.PaymentTime)).ToList();

            Double totalPayments = tickets.Sum(t => t.Price);

            IEnumerable<IGrouping<int, Ticket>> paymentsByHours = (from t in tickets
                                                                   group t by t.PaymentTime.Hour into g
                                                                   select g);

            return new TicketReportVO(totalPayments, paymentsByHours);

        }

        public Ticket parkCar(string id, string position)
        {
            Parking parking = this.Get(id);

            CarSpace carSpace = parking.CarSpaces.Where(cs => position.Equals(cs.Position)).FirstOrDefault();

            carSpace.IsBusy = true;

            Ticket ticket = new Ticket(DateTime.Now, DateTime.MaxValue, DateTime.MaxValue, id);

            this.ticketService.Insert(ticket);

            carSpace.TicketId = ticket.Id;

            this.Update(id, parking);

            return ticket;
        }

        public Ticket unparkCar(string id, string position)
        {
            Parking parking = this.Get(id);

            CarSpace carSpace = parking.CarSpaces.Where(cs => position.Equals(cs.Position)).FirstOrDefault();

            carSpace.IsBusy = false;

            Ticket ticket = this.ticketService.Get(carSpace.TicketId);

            ticket.EndDateTime = DateTime.Now;

            this.ticketService.Update(carSpace.TicketId, ticket);

            carSpace.TicketId = String.Empty;

            this.Update(id, parking);

            return ticket;
        }

    }
}
