﻿using rentcar.Service.Contract;
using rentcar.Repositories.Contract;
using rentcar.Model;
using System.Collections.Generic;

namespace rentcar.Service
{
    public class BaseService<T> : IService<T> where T : BaseEntity
    {

        protected IRepository<T> repository;

        public BaseService(IRepository<T> repository)
        {
            this.repository = repository;
        }

        public void Delete(string id)
        {
            this.repository.Delete(id);
        }

        public T Get(string id)
        {
            return this.repository.Get(id);
        }

        public IEnumerable<T> GetAll()
        {
            return this.repository.GetAll();
        }

        public virtual T Insert(T item)
        {
            return this.repository.Insert(item);
        }

        public virtual T Update(string id, T NewItem)
        {
            return this.repository.Update(id, NewItem);
        }
    }
}
