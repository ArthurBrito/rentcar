﻿using MongoDB.Driver;
using rentcar.Model;
using Microsoft.Extensions.Options;

namespace rentcar.database
{
    public class DatabaseContext
    {
        private static DatabaseContext singletonInstanceContext;
        private readonly IMongoDatabase _database = null;

        public DatabaseContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<CarSpace> CarSpaces
        {
            get
            {
                return _database.GetCollection<CarSpace>("CarSpaces");
            }
        }

        public IMongoCollection<Parking> Parkings
        {
            get
            {
                return _database.GetCollection<Parking>("Parkings");
            }
        }

        public IMongoCollection<Ticket> Tickets
        {
            get
            {
                return _database.GetCollection<Ticket>("Tickets");
            }
        }

        public IMongoCollection<TicketPrice> TicketsPrice
        {
            get
            {
                return _database.GetCollection<TicketPrice>("TicketsPrice");
            }
        }

        public static DatabaseContext getInstance(IOptions<Settings> settings)
        {
            if (singletonInstanceContext == null)
            {
                singletonInstanceContext = new DatabaseContext(settings);
            }
            return singletonInstanceContext;
        }

    }
}
