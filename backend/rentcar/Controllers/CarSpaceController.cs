﻿using Microsoft.AspNetCore.Mvc;
using rentcar.Service;
using rentcar.Repositories.Contract;
using rentcar.Model;
using System.Collections.Generic;

namespace rentcar.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarSpaceController : RentcarControllerBase<CarSpace, CarSpaceService>
    {
        public CarSpaceController(IRepository<CarSpace> repository) : base(new CarSpaceService(repository))
        {
        }

        // GET api/values/5
        [HttpGet("notBusy")]
        public ActionResult<IEnumerable<CarSpace>> GetNotBusyCarSpaces()
        {
            return new ActionResult<IEnumerable<CarSpace>>(this.service.GetNotBusyCarSpaces());
        }

    }
}