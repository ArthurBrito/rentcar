﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rentcar.Service;
using rentcar.Repositories.Contract;
using rentcar.Model;

namespace rentcar.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : RentcarControllerBase<Ticket, TicketService>
    {
        public TicketController(IRepository<Ticket> repository, IRepository<TicketPrice> ticketPriceRepository) : base(new TicketService(repository, new TicketPriceService(ticketPriceRepository)))
        {
        }

        [HttpPut("{id}/payTicket")]
        public ActionResult<Ticket> PayTicket(String id, [FromBody] Double paymentValue)
        {
            return new ActionResult<Ticket>(this.service.PayTicket(id, paymentValue));
        }

    }
}