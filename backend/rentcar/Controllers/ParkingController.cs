﻿using System;
using Microsoft.AspNetCore.Mvc;
using rentcar.Service;
using rentcar.Repositories.Contract;
using rentcar.Model;
using rentcar.Model.VO;
using rentcar.Model.Request;

namespace rentcar.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : RentcarControllerBase<Parking, ParkingService>
    {
        public ParkingController(IRepository<Parking> repository, IRepository<CarSpace> carSpaceRepository, IRepository<Ticket> ticketRepository, IRepository<TicketPrice> ticketPriceRepository) : base(new ParkingService(repository, new CarSpaceService(carSpaceRepository), new TicketService(ticketRepository, new TicketPriceService(ticketPriceRepository)) ))
        {
        }

        // GET api/values/5
        [HttpGet("{id}/CarSpacesAvaliableCount")]
        public ActionResult<int> CarSpacesAvaliableCount(String id)
        {
            return new ActionResult<int>(this.service.CarSpacesAvaliableCount(id));
        }

        [HttpPut("{id}/parkCar")]
        public ActionResult<Ticket> parkCar(String id, [FromBody] ParkCarRequest parkCarRequest)
        {
            return this.service.parkCar(id, parkCarRequest.position);
        }

        [HttpPut("{id}/unparkCar")]
        public ActionResult<Ticket> unparkCar(String id, [FromBody] ParkCarRequest parkCarRequest)
        {
            return this.service.unparkCar(id, parkCarRequest.position);
        }

        [HttpGet("{id}/paymentReport")]
        public ActionResult<TicketReportVO> PaymentReport(String id)
        {
            return this.service.PaymentReport(id);
        }

    }
}