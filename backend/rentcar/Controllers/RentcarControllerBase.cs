﻿using Microsoft.AspNetCore.Mvc;
using rentcar.Service.Contract;
using rentcar.Repositories.Contract;
using rentcar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentcar.Controllers
{
    public class RentcarControllerBase<T, S> : ControllerBase where T : BaseEntity where S : IService<T>
    {
        protected readonly S service;

        public RentcarControllerBase(S service)
        {
            this.service = service;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<T>> Get()
        {
            return new ActionResult<IEnumerable<T>>(this.service.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(String id)
        {
            return new ActionResult<T>(this.service.Get(id));
        }

        // POST api/values
        [HttpPost]
        public ActionResult<T> Post([FromBody] T item)
        {
            return new ActionResult<T>(this.service.Insert(item));
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult<T> Put(String id, [FromBody] T NewItem)
        {
            return new ActionResult<T>(this.service.Update(id, NewItem));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(String id)
        {
            this.service.Delete(id);
        }

    }
}
