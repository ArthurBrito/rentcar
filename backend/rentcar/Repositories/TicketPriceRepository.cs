﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using rentcar.database;
using rentcar.Model;
using rentcar.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rentcar.Repositories
{
    public class TicketPriceRepository : IRepository<TicketPrice>
    {

        private readonly DatabaseContext _context = null;

        public TicketPriceRepository(IOptions<Settings> settings)
        {
            _context = DatabaseContext.getInstance(settings);
        }

        public void Delete(string id)
        {
            try
            {
                _context.TicketsPrice.DeleteOne(Builders<TicketPrice>.Filter.Eq("Id", id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TicketPrice Get(string id)
        {
            try
            {
                return _context.TicketsPrice
                                .Find(tp => tp.Id == id)
                                .FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TicketPrice GetByTimeDescription(String timeDescription)
        {
            try
            {
                return _context.TicketsPrice
                                .Find(tp => timeDescription.Equals(tp.timeDescription))
                                .FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<TicketPrice> GetAll()
        {
            try
            {
                return _context.TicketsPrice.Find(new BsonDocument()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TicketPrice Insert(TicketPrice item)
        {
            try
            {
                _context.TicketsPrice.InsertOne(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return item;
        }

        public TicketPrice Update(string id, TicketPrice NewItem)
        {
            try
            {
                _context.TicketsPrice.ReplaceOne(Builders<TicketPrice>.Filter.Eq("Id", id), NewItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return NewItem;
        }

    }
}
