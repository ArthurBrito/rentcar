﻿using rentcar.Model;
using System;
using System.Collections.Generic;

namespace rentcar.Repositories.Contract
{
    public interface IRepository <T> where T : BaseEntity
    {

        IEnumerable<T> GetAll();

        T Get(String id);

        T Insert(T item);

        T Update(String id, T NewItem);

        void Delete(String id);

    }
}
