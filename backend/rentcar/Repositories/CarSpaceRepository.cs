﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using rentcar.database;
using rentcar.Repositories.Contract;
using rentcar.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar.Repositories
{
    public class CarSpaceRepository : IRepository<CarSpace>
    {

        private readonly DatabaseContext _context = null;

        public CarSpaceRepository(IOptions<Settings> settings)
        {
            _context = DatabaseContext.getInstance(settings);
        }

        public void Delete(string id)
        {
            try
            {
                _context.CarSpaces.DeleteOne(Builders<CarSpace>.Filter.Eq("Id", id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CarSpace Get(string id)
        {
            try
            {
                return _context.CarSpaces
                                .Find(car => car.Id == id)
                                .FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CarSpace> GetAll()
        {
            try
            {
                return _context.CarSpaces.Find(new BsonDocument()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CarSpace Insert(CarSpace item)
        {
            try
            {
                _context.CarSpaces.InsertOne(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return item;
        }

        public CarSpace Update(string id, CarSpace NewItem)
        {
            try
            {
                _context.CarSpaces.ReplaceOne(Builders<CarSpace>.Filter.Eq("Id", id), NewItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return NewItem;
        }
    }
}
