﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using rentcar.database;
using rentcar.Repositories.Contract;
using rentcar.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar.Repositories
{
    public class ParkingRepository : IRepository<Parking>
    {

        private readonly DatabaseContext _context = null;

        public ParkingRepository(IOptions<Settings> settings)
        {
            _context = DatabaseContext.getInstance(settings);
        }

        public void Delete(string id)
        {
            try
            {
                _context.Parkings.DeleteOne(Builders<Parking>.Filter.Eq("Id", id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Parking Get(string id)
        {
            try
            {
                return _context.Parkings
                                .Find(car => car.Id == id)
                                .FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Parking> GetAll()
        {
            try
            {
                return _context.Parkings.Find(new BsonDocument()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Parking Insert(Parking item)
        {
            try
            {
                _context.Parkings.InsertOne(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return item;
        }

        public Parking Update(string id, Parking NewItem)
        {
            try
            {
                _context.Parkings.ReplaceOne(Builders<Parking>.Filter.Eq("Id", id), NewItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return NewItem;
        }
    }
}
