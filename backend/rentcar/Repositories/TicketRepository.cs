﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using rentcar.database;
using rentcar.Repositories.Contract;
using rentcar.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar.Repositories
{
    public class TicketRepository : IRepository<Ticket>
    {

        private readonly DatabaseContext _context = null;

        public TicketRepository(IOptions<Settings> settings)
        {
            _context = DatabaseContext.getInstance(settings);
        }

        public void Delete(string id)
        {
            try
            {
                _context.Tickets.DeleteOne(Builders<Ticket>.Filter.Eq("Id", id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Ticket Get(string id)
        {
            try
            {
                return _context.Tickets
                                .Find(car => car.Id == id)
                                .FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Ticket> GetTicketsOfParking(String parkingId)
        {
            try
            {
                return _context.Tickets
                                .Find(t => parkingId.Equals(t.ParkingId))
                                .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Ticket> GetAll()
        {
            try
            {
                return _context.Tickets.Find(new BsonDocument()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Ticket Insert(Ticket item)
        {
            try
            {
                _context.Tickets.InsertOne(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return item;
        }

        public Ticket Update(string id, Ticket NewItem)
        {
            try
            {
                _context.Tickets.ReplaceOne(Builders<Ticket>.Filter.Eq("Id", id), NewItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return NewItem;
        }

    }
}
