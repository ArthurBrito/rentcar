﻿using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using rentcar.database;
using rentcar.Model;
using rentcar.Repositories;
using rentcar.Service;
using rentcar.Service.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar_test.Service
{
    [TestClass]
    public class TicketServiceTest
    {

        private Mock<IService<Ticket>> service;

        public TicketServiceTest()
        {
            IEnumerable<Ticket> ticketsList = new List<Ticket>();

            service = new Mock<IService<Ticket>>();
            service.Setup(t => t.GetAll()).Returns(ticketsList);
        }

        [TestMethod]
        public void getAllTest()
        {

            List<Ticket> tickets = service.Object.GetAll() as List<Ticket>;

            Assert.IsTrue(tickets.Count == 0);
        }

    }
}
