﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using rentcar.Model;
using rentcar.Service.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar_test.Service
{
    [TestClass]
    public class CarSpaceServiceTest
    {

        private Mock<IService<CarSpace>> service;

        public CarSpaceServiceTest()
        {
            IEnumerable<CarSpace> ticketsList = new List<CarSpace>();

            service = new Mock<IService<CarSpace>>();
            service.Setup(t => t.GetAll()).Returns(ticketsList);
        }

        [TestMethod]
        public void getAllTest()
        {

            List<CarSpace> carSpaces = service.Object.GetAll() as List<CarSpace>;

            Assert.IsTrue(carSpaces.Count == 0);
        }

    }
}
