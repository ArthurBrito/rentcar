﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using rentcar.Model;
using rentcar.Service.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar_test.Service
{
    [TestClass]
    public class ParkingServiceTest
    {

        private Mock<IService<Parking>> service;

        public ParkingServiceTest()
        {
            IEnumerable<Parking> ticketsList = new List<Parking>();

            service = new Mock<IService<Parking>>();
            service.Setup(t => t.GetAll()).Returns(ticketsList);
        }

        [TestMethod]
        public void getAllTest()
        {

            List<Parking> parkings = service.Object.GetAll() as List<Parking>;

            Assert.IsTrue(parkings.Count == 0);
        }

    }
}
