﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using rentcar.Model;
using rentcar.Service.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar_test.Service
{
    [TestClass]
    public class TicketPriceServiceTest
    {

        private Mock<IService<TicketPrice>> service;

        public TicketPriceServiceTest()
        {
            IEnumerable<TicketPrice> ticketsList = new List<TicketPrice>();

            service = new Mock<IService<TicketPrice>>();
            service.Setup(t => t.GetAll()).Returns(ticketsList);
        }

        [TestMethod]
        public void getAllTest()
        {

            List<TicketPrice> ticketsPrice = service.Object.GetAll() as List<TicketPrice>;

            Assert.IsTrue(ticketsPrice.Count == 0);
        }

    }
}
