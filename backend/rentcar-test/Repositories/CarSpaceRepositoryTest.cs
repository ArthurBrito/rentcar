﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using rentcar.Model;
using rentcar.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar_test.Repositories
{
    [TestClass]
    public class CarSpaceRepositoryTest
    {

        private Mock<IRepository<CarSpace>> service;

        public CarSpaceRepositoryTest()
        {
            IEnumerable<CarSpace> ticketsList = new List<CarSpace>();

            service = new Mock<IRepository<CarSpace>>();
            service.Setup(t => t.GetAll()).Returns(ticketsList);
        }

        [TestMethod]
        public void getAllTest()
        {
            List<CarSpace> carSpaces = service.Object.GetAll() as List<CarSpace>;
            Assert.IsTrue(carSpaces.Count == 0);
        }

    }
}
