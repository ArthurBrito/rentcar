﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using rentcar.Model;
using rentcar.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar_test.Repositories
{
    [TestClass]
    public class ParkingRepositoryTest
    {

        private Mock<IRepository<Parking>> service;

        public ParkingRepositoryTest()
        {
            IEnumerable<Parking> ticketsList = new List<Parking>();

            service = new Mock<IRepository<Parking>>();
            service.Setup(t => t.GetAll()).Returns(ticketsList);
        }

        [TestMethod]
        public void getAllTest()
        {
            List<Parking> parkings = service.Object.GetAll() as List<Parking>;
            Assert.IsTrue(parkings.Count == 0);
        }

    }
}
