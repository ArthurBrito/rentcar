﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using rentcar.Model;
using rentcar.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar_test.Repositories
{
    [TestClass]
    public class TicketPriceRepositoryTest
    {

        private Mock<IRepository<TicketPrice>> service;

        public TicketPriceRepositoryTest()
        {
            IEnumerable<TicketPrice> ticketsList = new List<TicketPrice>();

            service = new Mock<IRepository<TicketPrice>>();
            service.Setup(t => t.GetAll()).Returns(ticketsList);
        }

        [TestMethod]
        public void getAllTest()
        {
            List<TicketPrice> ticketPrices = service.Object.GetAll() as List<TicketPrice>;
            Assert.IsTrue(ticketPrices.Count == 0);
        }

    }
}
