﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using rentcar.Model;
using rentcar.Model.VO;
using System;
using System.Collections.Generic;
using System.Text;

namespace rentcar_test.Model
{
    [TestClass]
    public class ParkingTest
    {

        [TestMethod]
        public void createParking()
        {
            GeolocationVO geolocationVO = new GeolocationVO();

            geolocationVO.Latitude = -7.1172327;
            geolocationVO.Longitude = -34.8293001;

            Parking parking = new Parking("NOME DE ESTACIONAMENTO TESTE", geolocationVO, null);

            Assert.IsNotNull(parking);
        }

    }
}
