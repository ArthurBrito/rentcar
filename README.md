# **Desafio do Estacionamento de Carros**

# Motivação
Avaliar o desempenho de desenvolvedores ao utilizar o framework ASP.NET Core para implementar uma API RESTful documentada e testada.

# Introdução 
Você foi procurado para implementar uma API que gerencie um estaciomento privado de veículos. 

O cliente precisa que o sistema seja feito em ASP.NET Core já que ele é produtivo, eficiente e multi-plataforma.

# Descrição do sistema
Criar uma API para gerenciamento de carros em um estacionamento, que deve ter pelo menos 15 vagas. 
Através dos métodos da API será possível ver quantas vagas estão disponíveis, estacionar um carro, realizar o pagamento do ticket e emitir um relatório de recebimentos.
|Permanência|Valor (R$)|
|-----------|---------:|
|Até 3 horas|7,00      |
|Hora extra |3,00      |

Métodos a serem criados:
- Consultar quantidade de vagas disponíveis
- Listar posição das vagas disponíveis
- Estacionar um carro numa vaga
- Pagar ticket
- Relatório com ocupação atual do estacionamento
- Relatório com valor arrecadado por período

**Caso algum aspecto do problema não tenha sido detalhado, solucione da forma que achar melhor, e justifique sua decisão.**

# Tecnologias requeridas
- ASP.NET Core (C#) rodando em Windows ou Linux
- Banco de dados à sua escolha (relacional ou não)
- Testes unitários em todos os métodos da API
- Documentação dos métodos com Swagger/OpenAPI
- *(extra)* CI/CD
- *(extra)* Monitoramento da API
- *(extra)* FrontEnd em um framework à sua escolha

# Como entregar o desafio?
Faça um fork desse repositório e mande o link de clonagem para anderson@vsoft.com.br.
Troque o item final (*Como entregar o desafio?*) por *Detalhe da solução*, onde você explicará as decisões de arquitetura que tomou e como executar seu projeto.
Depois você será convocado para apresentar sua solução presencialmente.

# **Entrega**

Api construída utilizando ASP.NET core 2.2, integrando-se com o banco de dados não relacional Mongo DB, possuindo features parciais também em uma interface desenvolvida em Angular 7. Além disto, como um grande interessado na cultura DevOps, apliquei CI/CD utilizando imagens containerizadas com docker e também o Deploy continuo, através da Azure.

# Rentcar API

- Repositório: https://bitbucket.org/ArthurBrito/rentcar/src/master/backend
- API publicada: https://rentcar-api.azurewebsites.net

# Rentcar Frontend

- Repositório: https://bitbucket.org/ArthurBrito/rentcar/src/master/frontend

# Rentcar Database (Mongo DB)
- Instância: mongodb+srv://aviana:<>@cluster0-cbvup.gcp.mongodb.net/test

# Tecnologias Utilizadas
- ASP.NET Core (C#) rodando em Windows container na Azure
- Banco de dados Mongo DB (Não relacional)
- Testes unitários utilizando a ferramenta MSTest
- Documentação dos métodos com Swagger
- CI/CD através do repositório do bitbucket
- Monitoramento da API através da Azure
- FrontEnd desenvolvido em angular (parcialmente completo)

# Tecnologias Criados
- Consultar quantidade de vagas disponíveis (Backend e Front)
- Listar posição das vagas disponíveis (Backend e Front)
- Estacionar um carro numa vaga (Backend e Front)
- Pagar ticket (Backend)
- Relatório com ocupação atual do estacionamento (Backend)
- Relatório com valor arrecadado por período (Backend)

# Padrões de projeto

- MVC
- Baixo acoplamento
- Alta coesão
- Singleton
- Clean Code

# Evidencias 

![](/evidencias/frontend1.png)
![](/evidencias/backend1.png)
![](/evidencias/CICD.png)
![](/evidencias/mongodb.png)
![](/evidencias/monitoramento.png)
![](/evidencias/testes-unitarios.png)